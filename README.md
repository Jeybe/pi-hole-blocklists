# This repo was moved
This repo was moved to [spootle/blocklist](https://codeberg.org/spootle/blocklist/). If you intend to use the blocklist or any other files from this project, please move on to the new repo, or if you already use this project, please switch to the new repo.
**Nevertheless, don't panic.** This repo will still get updated for some while, alongside with the new repo. So take your time, switch if you can and everything will be fine. I hope you enjoy using this blocklist.

# Pi-Hole blocklist

This blocklist was created by merging several existing blocklists into one. All files used are listed in `blocklist-files-urls.txt`. The final blocklist file is `blocklist.txt`.

**Add the list to your Pi-Hole**

* [Blocklist file: raw link](https://codeberg.org/spootle/blocklist/raw/branch/master/blocklist.txt)

**Update interval:** daily

## What's on the list?

In short: Domains that deliver **tracker, ads and malware**.
The domains are collected and merged into one file by my [Pi-Hole merger script](https://codeberg.org/Jeybe/bash-scripts/src/branch/master/scripts/ph-merger).

**The goal** of this list is to make it possible to identify and **block unwanted and harmful items** in the internet. Effectively the domains on this list are known to deliver exactly these unwanted and harmful items, such as **tracking and analytic scripts**. As **ads** are often loaded from third party providers which are **spying** on users too, the domains from such providers are also included in this list. Obviously, **malware** also counts directly as harmful and unwanted, which is why such domains are also blocked.

**New blocklists** will be added if I stumble across and consider them as useful to the goal of this list.

## Overblocking

* As less as possible

This Blocklist is intended to be a **easy to use blocklist**. This means to block **as many as possible unwanted domains** while **reduce overblocking to a minimum level**. In the end you should get a solid blocklist, that doesn't bother you with making half of the internet unavailable. For some of you this might be to less, what means, that you have to take extra steps to ensure that everything that should be blocked is blocked. Nevertheless this blocklist is a good starting point.

### Whoops - overblocking is happening anyway

* Check out the [whitelist](whitelist.md)

For those of you who have issues with overblocking there is a [whitelist](whitelist.md) with domains that get blocked with this blocklist for good reasons, but may break services you use. Just pick out the ones you need and add exeptions for them in your Pi-Hole. Also, you might consider why you are dependent from services that possibly track you, or serve other unwanted things.

## Which blocklists were unified - Whom to thank?

Following is a table with all blocklist files that were used. A **huge thanks** to everyone who contributes to and maintains these blocklists. Thanks to them and their great work there is an easy way to avoid unwanted things on the internet!

#### Thank you!

| Name | Category | Project overview | RAW link | Licence |
| ---- | ----------- | ------- | -------- | ------- |
| add.207Net | Analytics | [Repository](https://github.com/FadeMind/hosts.extras) | [RAW file (unknown if official or not)](https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.2o7Net/hosts) | GPLv3 |
| Disconnect ad filter list | Ads and analytics | [Website](https://disconnect.me) | [RAW file (unknown if official or not)](https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt) | GPLv3 |
| Disconnect malvertising list | Malware | [Website](https://disconnect.me) | [RAW file (unknown if official or not)](https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt) | GPLv3 |
| EasyList | Ads | [Website](https://easylist.to/) | [RAW file (unofficial)](https://v.firebog.net/hosts/Easylist.txt) | GPLv3 / CC BY-SA 3.0 |
| EasyPrivacy | Analytics | [Website](https://easylist.to/) | [RAW file (unofficial)](https://v.firebog.net/hosts/Easyprivacy.txt) | GPLv3 / CC BY-SA 3.0 |
| NoTrack Tracker blocklist | Analytics | [Repository](https://gitlab.com/quidsup/notrack-blocklists) | [RAW file (official)](https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-blocklist.txt) | GPLv3 |
| NoTrack Tracker Malware | Malware | [Repository](https://gitlab.com/quidsup/notrack-blocklists) | [RAW file (official)](https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-malware.txt) | GPLv3 |
| AnudeepND adservers blacklist | Ads and analytics | [Repository](https://github.com/anudeepND/blacklist) | [RAW file (official)](https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt) | MIT Licence |
| AnudeepND coinminer blacklist | Malware | [Repository](https://github.com/anudeepND/blacklist) | [RAW file (official)](https://raw.githubusercontent.com/anudeepND/blacklist/master/CoinMiner.txt) | MIT Licence |
| NoCoin filter list | Malware | [Repository](https://github.com/hoshsadiq/adblock-nocoin-list) | [RAW file (official)](https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/hosts.txt) | MIT Licence |
| Perflyst PiHoleBlocklist android tracking | Analytics | [Repository](https://github.com/Perflyst/PiHoleBlocklist/) | [RAW file (official)](https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/android-tracking.txt) | MIT Licence |
| Perflyst PiHoleBlocklist SmartTV | analytics | [Repository](https://github.com/Perflyst/PiHoleBlocklist/) | [RAW file (official)](https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/SmartTV.txt) | MIT Licence |
| WindowsSpyBlocker Spy Blocklist | analytics | [Repository](https://github.com/crazy-max/WindowsSpyBlocker) | [RAW file (official)](https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt) | MIT Licence |
| AdAway default blocklist | Ads and analytics | [Website](https://adaway.org) | [RAW file (official)](https://adaway.org/hosts.txt) | CC Attribution 3.0 |
| Airelle trc | Ads | [Website](http://rlwpx.free.fr/WPFF/hosts.htm) | [RAW file (unofficial)](https://v.firebog.net/hosts/Airelle-trc.txt) | CC BY-NC 3.0 |
| Gameindustry cities | Ads and analytics | [Website](https://gameindustry.de/hosts.php) | [RAW file (official)](https://gameindustry.de/files/staedte/hosts) | CC BY-SA 4.0 |
| Prigent Ads | Ads | [Website](https://dsi.ut-capitole.fr/blacklists/index_en.php) | [RAW file (unofficial)](https://v.firebog.net/hosts/Prigent-Ads.txt) | CC BY-SA 4.0 |
| Prigent Malware | Malware | [Website](https://dsi.ut-capitole.fr/blacklists/index_en.php) | [RAW file (unofficial)](https://v.firebog.net/hosts/Prigent-Malware.txt) | CC BY-SA 4.0 |
| DNS-BH Malware | Malware | [Website](https://www.malwaredomains.com/) | [RAW file (official)](https://mirror1.malwaredomains.com/files/justdomains) | "This malware block lists provided here are for free for noncommercial use as part of the fight against malware." |
| Malware Domain List | Malware | [Website](https://www.malwaredomainlist.com/) | [RAW file (official)](https://www.malwaredomainlist.com/hostslist/hosts.txt) | "Our list can be used for free by anyone. Feel free to use it." |
| Squidblacklist dg-ads | Ads | [Website](https://www.squidblacklist.org/) | [RAW file (official)](https://www.squidblacklist.org/downloads/dg-ads.acl) | "You may freely use, copy, and redistribute this blacklist in any manner you like." |
| Squidblacklist dg-malicious | Malware | [Website](https://www.squidblacklist.org/) | [RAW file (official)](https://www.squidblacklist.org/downloads/dg-malicious.acl) | "You may freely use, copy, and redistribute this blacklist in any manner you like." |
| hpHosts adservers | Ads and Analytics | [Website](https://hosts-file.net) | [RAW file (official)](https://hosts-file.net/ad_servers.txt) | "This service is free to use, however, any and ALL automated use is strictly forbidden without express permission from ourselves" |
| hpHosts Exploit | Malware | [Website](https://hosts-file.net) | [RAW file (official)](https://hosts-file.net/exp.txt) | "This service is free to use, however, any and ALL automated use is strictly forbidden without express permission from ourselves" |
| hpHosts Malware | Malware | [Website](https://hosts-file.net) | [RAW file (official)](https://hosts-file.net/emd.txt) | "This service is free to use, however, any and ALL automated use is strictly forbidden without express permission from ourselves" |
| Akamaru Pi-Hole-Lists Appads | Ads | [Repository](https://github.com/Akamaru/Pi-Hole-Lists/) | [RAW file (official)](https://raw.githubusercontent.com/Akamaru/Pi-Hole-Lists/master/mobile.txt) | no licence |
| Akamaru Pi-Hole-Lists HbbTV | Ads | [Repository](https://github.com/Akamaru/Pi-Hole-Lists/) | [RAW file (official)](https://raw.githubusercontent.com/Akamaru/Pi-Hole-Lists/master/smarttv.txt) | no licence |
| Akamaru Pi-Hole-Lists nomsdata | Analytics | [Repository](https://github.com/Akamaru/Pi-Hole-Lists/) | [RAW file (official)](https://raw.githubusercontent.com/Akamaru/Pi-Hole-Lists/master/nomsdata.txt) | no licence |
| Akamaru Pi-Hole-Lists cryptomine | Malware | [Repository](https://github.com/Akamaru/Pi-Hole-Lists/) | [RAW file (official)](https://raw.githubusercontent.com/Akamaru/Pi-Hole-Lists/master/cryptomine.txt) | no licence |
| AdGuard DNS | Ads and analytics | [Website](https://adguard.com) | [RAW file (unofficial)](https://v.firebog.net/hosts/AdguardDNS.txt) | unknown |
| UncheckyAds | Ads | [Repository](https://github.com/FadeMind/hosts.extras) | [RAW file (unknown if official or not)](https://raw.githubusercontent.com/FadeMind/hosts.extras/master/UncheckyAds/hosts) | unknown |

## Licence
The whole repository and the files in it are licenced under the **GPLv3**. A copy can be found [here](LICENCE).
